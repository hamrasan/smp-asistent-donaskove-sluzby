# SMP - Asistent donáškové služby
### DOKUMENTY K OPONENTURE
[01 Analyza (PDF)](https://gitlab.fel.cvut.cz/zecmarce/smp-asistent-donaskove-sluzby/blob/master/AsistentDonaskoveSluzby_analyza.pdf)
<del>[01 Analyza](https://docs.google.com/document/d/1auO4xVD26yA9TuEi3nVc4CZhk1Fa5xRpjF4dkGWdFMc/edit?usp=sharing)</del>


<hr>

### PREZENTACE
[Prezentácia o projekte](https://docs.google.com/presentation/d/1FNJfnNtMaQ4cnS8ikpUSVEpPoZFinRfCKnU7xlmp-MY/edit?usp=sharing)

[Prvni tyden](https://docs.google.com/presentation/d/1c6qTEJI0AXNwUNfu3pSEVfEmE5WqeP0WUcBwA1iHHeI/edit?usp=sharing)

<hr>

### INE DOKUMENTY

[Pracovna verzia dokumentu](https://docs.google.com/document/d/1nVAc2N6DnUkh1hGiWgo4Vtp4Ibz6rNOndauQDP3FJd0/edit?usp=sharing)

[Výkaz práce](vykaz_prace.md)
<hr>

### ZAPISKY ZE CVICENI
[cviceni 4.3.](https://docs.google.com/document/d/1o6q4BVdJv9o_E9Phh11zZO6q1GhHTQJCXCcyzNaRaT4/edit?usp=sharing)
