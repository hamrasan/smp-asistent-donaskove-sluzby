# Vykaz prace

#### Sandra Hamrakova

| datum | popis | cas |
| ------ | ------ |  ------ |
|1. tyden| | |
|02.3. |analyza - zakaznik,stakeholders |30 min |
|03.3. | diskusia - zmeny |15 min |
|2. tyden| | |
|06.3.|stretnutie - BDM,BPM| 1h 30 min|
<hr>

#### Lukas Simon

| datum | popis | cas |
| ------ | ------ |  ------ |
|1. tyden|  |  |
|2.3. |Finanční analýza |40min |
|3.3 |diskuze - úpravy |15 min |
| 2. tyden |  |  |
|06.3.|stretnutie - BDM,BPM| 1h 30 min|
<hr>

#### Josef Vyskocil

| datum | popis | cas |
| ------ | ------ |  ------ |
|1. tyden|  |  |
|03.03. |výstupy |30 min |
|03.03. |rizika  |10 min |
|03.03.| diskuze - upravy|5 min|
|04.03.|záznam ze cvičení|10 min|
| 2. tyden |  |  |
|06.3.|stretnutie - BDM,BPM| 1h 30 min|
|09.03.|Business model|15 min|
|10.03.|Oponentura|20 min|
<hr>

#### Marcel Zec

| datum | popis | cas |
| ------ | ------ |  ------ |
|1. tyden|  |  |
|26.2. |zalozenie Git |10 min |
| 02.3.|sablona dokumentu | 10 min |
| 02.3.|analyza - uvod,popis,zadavatel | 40 min |
|03.3.|diskusia - zmeny|15 min|
|03.3.|ciele|15 min|
|03.3.|prezentacia na cviko|20 min|
| 2. tyden |  |  |
|06.3.|stretnutie - BDM,BPM| 1h 30 min|